function SectionInfo() {

    // Sections
    let sections = [
        {
            section_id: 1,
            sec_section: "Saint Francis",
            sec_lastupdate: "2022-08-01 11:54:11"
        },
        {
            section_id: 2,
            sec_section: "Saint Paul",
            sec_lastupdate: "2022-08-03 11:54:11"
        },
        {
            section_id: 3,
            sec_section: "Saint Therese",
            sec_lastupdate: "2022-08-04 11:54:11"
        },
        {
            section_id: 4,
            sec_section: "Saint John",
            sec_lastupdate: "2022-08-05 11:54:11"
        },
        {
            section_id: 5,
            sec_section: "Saint Peter",
            sec_lastupdate: "2022-08-06 11:54:11"
        },

        {
            section_id: 6,
            sec_section: "Saint Luke",
            sec_lastupdate: "2022-08-07 11:54:11"
        }
    ];


    return(
        sections.map(({section_id, sec_section, sec_lastupdate}) => (
              
            // input here.
            <p key={section_id}>
                Section id: {section_id}
                <br></br>
                Section name: {sec_section}
                <br></br>
                Section last updated: {sec_lastupdate}
                <br></br>
            </p>
        ))
    );

}

export default SectionInfo;