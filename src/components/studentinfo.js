function StudentInfo() {

        // student info
    let student = [
        {
            id: 1,
            stud_firstname: "Paul Wisdom",
            stud_lastname: "Samson",
            stud_middlename: "Cabayacruz",
            stud_address: "Zamboanga",
            stud_age: 23,
            stud_zip: 7000,
            stud_city: "Zamboanga City",
            stud_regionProvince: "Region 9",
            stud_phoneNo: "09999998542",
            stud_mobileNo: "09999998542",
            stud_email: "paulwisdom@gmail.com",
            stud_yrlvl: 4,
            stud_section: 2,
            sec_UpdatedAt: "2022-08-05 12:55:48",

        },
        {
            id: 2,
            stud_firstname: "Samuel",
            stud_lastname: "Guban",
            stud_middlename: "A",
            stud_address: "Camarines Norte",
            stud_age: 24,
            stud_zip: 4700,
            stud_city: "Talisay",
            stud_regionProvince: "Region 6",
            stud_phoneNo: "09994345555",
            stud_mobileNo: "099943455567",
            stud_email: "samuelguban@gmail.com",
            stud_yrlvl: 3,
            stud_section: 1,
            sec_UpdatedAt: "2022-08-05 11:35:48",

        },
        {
            id: 3,
            stud_firstname: "Lawrence",
            stud_lastname: "Chan",
            stud_middlename: "A",
            stud_address: "Bulacan",
            stud_age: 25,
            stud_zip: 3004,
            stud_city: "Plaridel",
            stud_regionProvince: "Region 3",
            stud_phoneNo: "0999958426",
            stud_mobileNo: "09900098542",
            stud_email: "lawrence@gmail.com",
            stud_yrlvl: 2,
            stud_section: 5,
            sec_UpdatedAt: "2022-08-05 11:45:48",

        },
        {
            id: 4,
            stud_firstname: "Mico",
            stud_lastname: "Gelle",
            stud_middlename: "B",
            stud_address: "Cavite",
            stud_age: 25,
            stud_zip: 4001,
            stud_city: "Imus",
            stud_regionProvince: "Region 4",
            stud_phoneNo: "0999913248",
            stud_mobileNo: "09979998542",
            stud_email: "mico@gmail.com",
            stud_yrlvl: 1,
            stud_section: 6,
            sec_UpdatedAt: "2022-08-05 11:25:48",

        },
        {
            id: 5,
            stud_firstname: "Juergen",
            stud_lastname: "Bautista",
            stud_middlename: "Roi",
            stud_address: "Tugegarao",
            stud_age: 26,
            stud_zip: 1007,
            stud_city: "Tugegarao",
            stud_regionProvince: "Region 2",
            stud_phoneNo: "09990098542",
            stud_mobileNo: "099789998542",
            stud_email: "juergen@gmail.com",
            stud_yrlvl: 2,
            stud_section: 1,
            sec_UpdatedAt: "2022-08-05 11:50:48",

        },
        {
            id: 6,
            stud_firstname: "Jed",
            stud_lastname: "Caturan",
            stud_middlename: "F",
            stud_address: "Rizal",
            stud_age: 23,
            stud_zip: 7458,
            stud_city: "Antipolo",
            stud_regionProvince: "Region 4",
            stud_phoneNo: "099947898542",
            stud_mobileNo: "09119998542",
            stud_email: "jed@gmail.com",
            stud_yrlvl: 1,
            stud_section: 5,
            sec_UpdatedAt: "2022-08-05 11:59:48",

        },
        {
            id: 7,
            stud_firstname: "Adrian",
            stud_lastname: "Mandac",
            stud_middlename: "M",
            stud_address: "Quezon City",
            stud_age: 24,
            stud_zip: 7892,
            stud_city: "Novaliches",
            stud_regionProvince: "NCR",
            stud_phoneNo: "09922998542",
            stud_mobileNo: "09239998542",
            stud_email: "adrian@gmail.com",
            stud_yrlvl: 4,
            stud_section: 1,
            sec_UpdatedAt: "2022-08-04 11:55:48",

        },
        {
            id: 8,
            stud_firstname: "Airon",
            stud_lastname: "Eusebio",
            stud_middlename: "Noel",
            stud_address: "Bulacan",
            stud_age: 27,
            stud_zip: 3008,
            stud_city: "San Ildefonso",
            stud_regionProvince: "Region 3",
            stud_phoneNo: "099978998542",
            stud_mobileNo: "09955998542",
            stud_email: "Airon@gmail.com",
            stud_yrlvl: 2,
            stud_section: 5,
            sec_UpdatedAt: "2022-08-04 11:55:48",

        },
        {
            id: 9,
            stud_firstname: "Henry",
            stud_lastname: "Conico",
            stud_middlename: "Dean",
            stud_address: "Pasig",
            stud_age: 18,
            stud_zip: 8792,
            stud_city: "Pasig",
            stud_regionProvince: "NCR",
            stud_phoneNo: "09978998542",
            stud_mobileNo: "09159998542",
            stud_email: "henry@gmail.com",
            stud_yrlvl: 1,
            stud_section: 6,
            sec_UpdatedAt: "2022-08-01 11:55:48",

        },
        {
            id: 10,
            stud_firstname: "Clinton",
            stud_lastname: "Amarante",
            stud_middlename: "Cadiz",
            stud_address: "Batangas",
            stud_age: 15,
            stud_zip: 2598,
            stud_city: "Talisay",
            stud_regionProvince: "Region 5",
            stud_phoneNo: "09999978542",
            stud_mobileNo: "09996698542",
            stud_email: "clinton@gmail.com",
            stud_yrlvl: 3,
            stud_section: 1,
            sec_UpdatedAt: "2022-08-08 11:55:48",

        },
        {
            id: 11,
            stud_firstname: "Ian",
            stud_lastname: "Tolentino",
            stud_middlename: "Cruz",
            stud_address: "Nueva Ecija",
            stud_age: 28,
            stud_zip: 7820,
            stud_city: "Gapan",
            stud_regionProvince: "Region 4",
            stud_phoneNo: "09948998542",
            stud_mobileNo: "09908998542",
            stud_email: "ian@gmail.com",
            stud_yrlvl: 2,
            stud_section: 6,
            sec_UpdatedAt: "2022-08-09 11:55:48",

        },
        {
            id: 12,
            stud_firstname: "Erick",
            stud_lastname: "Cruz",
            stud_middlename: "Perez",
            stud_address: "Cebu",
            stud_age: 17,
            stud_zip: 7010,
            stud_city: "Mactan",
            stud_regionProvince: "Region 8",
            stud_phoneNo: "099774998542",
            stud_mobileNo: "09224998542",
            stud_email: "erick@gmail.com",
            stud_yrlvl: 4,
            stud_section: 2,
            sec_UpdatedAt: "2022-08-07 11:55:48",

        },
        {
            id: 13,
            stud_firstname: "Mike",
            stud_lastname: "Banning",
            stud_middlename: "Z",
            stud_address: "Mindoro",
            stud_age: 27,
            stud_zip: 8200,
            stud_city: "Calapan",
            stud_regionProvince: "Region 5",
            stud_phoneNo: "09979998542",
            stud_mobileNo: "09894998542",
            stud_email: "mike@gmail.com",
            stud_yrlvl: 1,
            stud_section: 6,
            sec_UpdatedAt: "2022-08-10 11:55:48",

        },
        {
            id: 14,
            stud_firstname: "Rj",
            stud_lastname: "Santos",
            stud_middlename: "Lopez",
            stud_address: "Aklan",
            stud_age: 16,
            stud_zip: 2578,
            stud_city: "Kalibo",
            stud_regionProvince: "Region 7",
            stud_phoneNo: "09977998542",
            stud_mobileNo: "09945998542",
            stud_email: "rj@gmail.com",
            stud_yrlvl: 3,
            stud_section: 5,
            sec_UpdatedAt: "2022-08-10 11:55:48",

        },
        {
            id: 15,
            stud_firstname: "Jane",
            stud_lastname: "Malonzo",
            stud_middlename: "Abad",
            stud_address: "Laguna",
            stud_age: 19,
            stud_zip: 7852,
            stud_city: "Calamba",
            stud_regionProvince: "Region 4",
            stud_phoneNo: "09905998542",
            stud_mobileNo: "09889998542",
            stud_email: "jane@gmail.com",
            stud_yrlvl: 1,
            stud_section: 4,
            sec_UpdatedAt: "2022-08-11 11:55:48",

        },
        {
            id: 16,
            stud_firstname: "Ryan",
            stud_lastname: "Espejo",
            stud_middlename: "N",
            stud_address: "Caloocan",
            stud_age: 18,
            stud_zip: 7890,
            stud_city: "Caloocan",
            stud_regionProvince: "NCR",
            stud_phoneNo: "09125398542",
            stud_mobileNo: "09978998542",
            stud_email: "ryan@gmail.com",
            stud_yrlvl: 3,
            stud_section: 3,
            sec_UpdatedAt: "2022-08-12 11:55:48",

        },
        {
            id: 17,
            stud_firstname: "Sara",
            stud_lastname: "Santos",
            stud_middlename: "Dela Cruz",
            stud_address: "Makati",
            stud_age: 20,
            stud_zip: 4790,
            stud_city: "Makati",
            stud_regionProvince: "NCR",
            stud_phoneNo: "09997798542",
            stud_mobileNo: "09978998542",
            stud_email: "sara@gmail.com",
            stud_yrlvl: 4,
            stud_section: 5,
            sec_UpdatedAt: "2022-08-01 11:55:48",

            }
    ];

    return(
            student.map(({id, stud_firstname, stud_lastname, stud_middlename, stud_address, stud_age, stud_zip, stud_city, stud_regionProvince, stud_phoneNo, stud_mobileNo, stud_email, stud_yrlvl, stud_section, sec_UpdatedAt}) => (
                    <p key={id}>
                        Id: {id}
                        <br></br>
                        First name: {stud_firstname}
                        <br></br>
                        Last name: {stud_lastname}
                        <br></br>
                        Middle name: {stud_middlename}
                        <br></br>
                        Address: {stud_address}
                        <br></br>
                        Age: {stud_age}
                        <br></br>
                        ZIP: {stud_zip}
                        <br></br>
                        City: {stud_city}
                        <br></br>
                        Region/Province: {stud_regionProvince}
                        <br></br>
                        Phone no: {stud_phoneNo}
                        <br></br>
                        Mobile no: {stud_mobileNo}
                        <br></br>
                        Emmail: {stud_email}
                        <br></br>
                        Year level: {stud_yrlvl}
                        <br></br>
                        Section: {stud_section}
                        <br></br>
                        Section updated: {sec_UpdatedAt}
                        <br></br>
                    </p>

                ))
    );
}

export default StudentInfo;