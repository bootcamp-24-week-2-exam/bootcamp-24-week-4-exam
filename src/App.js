import './App.css';
import StudentInfo from './components/studentinfo';
// import SectionInfo from './components/sectioninfo';
import Crud from './components/crud';

function App() {
  return (
    <main>
    <StudentInfo />
    {/* <SectionInfo /> */}
    <Crud />
    </main>
  );
}

export default App;
